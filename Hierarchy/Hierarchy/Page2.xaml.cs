﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        void onCorgiClicked(object sender, EventArgs e)
        {
            corgi.IsVisible = !(corgi.IsVisible);
            if (corgi.IsVisible)
            {
                corgiButton.Text = "There he is!";
            }
            else
            {
                corgiButton.Text = "Where is the corgi?";
            }
        }
    }
}

