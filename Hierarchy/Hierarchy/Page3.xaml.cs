﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        int counter = 0;
        public Page3()
        {
            InitializeComponent();
        }

        // changes the horse on clicked
        void onHorseClicked (object sender, EventArgs e)
        {
            if (counter == 0)
            {
                horse.Source = "horse2.jpg";
                counter++;
            }
            else if (counter == 1)
            {
                horse.Source = "horse3.jpg";
                counter++;
            }
            else if (counter == 2)
            {
                horse.Source = "horse4.jpg";
                counter++;
            }
            else
            {
                horse.Source = "horse1.jpg";
                counter = 0;
            }
            }
        }
    }
