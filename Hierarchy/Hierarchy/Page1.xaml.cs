﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        int counter = 0;

        public Page1()
        {
            InitializeComponent();
        }

        // subtracts from the counter
        void onMinusClicked(object sender, EventArgs e)
        {
            counter--;
            counterHolder.Text = counter.ToString();
        }

        // adds to the counter
        void onAddClicked(object sender, EventArgs e)
        {
            counter++;
            counterHolder.Text = counter.ToString();
        }
    }
}