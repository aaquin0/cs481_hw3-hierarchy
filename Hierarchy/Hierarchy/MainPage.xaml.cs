﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void Handle_NavigateToPage1(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page1());
        }

        async void Handle_NavigateToPage2(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page2());
        }

        async void Handle_NavigateToPage3(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new Page3());
        }
    }
}
